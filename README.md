# Turtlesim Demo

This turtlesim demo runs an easy example demo on the computer using turtlesim. The demo exists out of a publisher and a subscriber. The publisher will generate a random number between 1 and 4 (this can be seen as a fake sensor input) and send it to the subscriber. The subscriber will transform this number into a movement for the turtle in turtlesim. The **result** is that the turtle in turtlesim starts moving randomly.

This is an exercise that the students can make to get a feel for how ROS works.

## How to run the demo

First you need to build the code.

```
colcon build
```

Lastly, in an other terminal source and run the code for the demo.

```
. install/setup.bash
ros2 launch fake_sensor launch.py
```

## Note

This demo was created in ROS 2 Foxy. Read the [documentation](https://docs.ros.org/en/foxy/index.html) for more information.