import rclpy
from rclpy.node import Node
from std_msgs.msg import Int8
from geometry_msgs.msg import Twist

class SensorToTurtle(Node):
    def __init__(self):
        super().__init__('sensor_to_turtle')

        self.subsription = self.create_subscription(Int8, 'topic/fake_sensor', self.listerner_callback, 10)
        self.publisher = self.create_publisher(Twist, 'turtle1/cmd_vel', 10)

    def listerner_callback(self, msg):

        sensor_data = msg.data

        if sensor_data == 1: # STRAIGHT
            linear, angular = 1.0, 0.0
            self.get_logger().info(f'Received data: {sensor_data} - Moving straight')
        elif sensor_data == 2: # RIGHT
            linear, angular = 0.0, -1.0
            self.get_logger().info(f'Received data: {sensor_data} - Turning right')
        elif sensor_data == 3: # LEFT
            linear, angular = 0.0, 1.0
            self.get_logger().info(f'Received data: {sensor_data} - Turning left')
        elif sensor_data == 4: # BACK
            linear, angular = -1.0, 0.0
            self.get_logger().info(f'Received data: {sensor_data} - Moving back')
        else:
            self.get_logger().info(f'Invalid sensor data: {sensor_data}')

        cmd = Twist()
        cmd.linear.x = linear
        cmd.angular.z = angular

        self.publisher.publish(cmd)

def main(args=None):
    rclpy.init(args=args)
    sensor_to_turtle = SensorToTurtle()

    rclpy.spin(sensor_to_turtle)

    sensor_to_turtle.destroy_node()

    rclpy.shutdown()

if __name__ == '__main__':
    main()
            