import launch
import launch_ros.actions

def generate_launch_description():
    return launch.LaunchDescription([
        launch_ros.actions.Node(
            package='fake_sensor',
            executable='sensor',
            name='sensor'),
        launch_ros.actions.Node(
            package='fake_sensor',
            executable='sensor_to_turtle',
            name='sensor_to_turtle'),
        launch_ros.actions.Node(
            package='turtlesim',
            executable='turtlesim_node',
            name='turtle'),
    ])